﻿using System;
namespace Assignment2
{
    public class Calculator
    {
        int[] numbers;
        public Calculator(int[] numbers)
        {
            this.numbers = numbers;
        }
        public int Sum()
        {
            int result = 0;
            foreach (int num in numbers) { result += num; }
            return result;
        }
        public int Subtraction(int x)
        {
            /* sum them first then subtract them
             * from a number that you should pass to the function
             */
            int z = Sum();
            return x - z;
        }
        public int Division(int divideby)
        {
            /* handle the following scenarios
             * if the numbers array was empty
             * the divideby was zero
            */
            if (numbers.Length==0 || divideby==0)
            {
                return -1;
            }
            return Sum() / divideby;
        }
        public int Multiplication(int multiplyby)
        {
            return sum() * multiplyby;
        }
    }
}
